import {Component, OnInit} from '@angular/core';
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {Angular2Csv} from "angular2-csv";
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {
  customers: any[] = [];
  toggleLoading: boolean;

  constructor(private lookup: LookupService, private utilities: UtilitiesService) {
  }

  ngOnInit() {
    this.toggleLoading = true;
    this._getCustomers();
  }

  _getCustomers() {
    this.lookup.getAllCustomers()
      .subscribe((customers) => {
          this.customers = customers;
          console.log(this.customers);
          this.toggleLoading = false;
        },
        err => {
          this.toggleLoading = false;
        })
  }

  printCustomer() {
    this.utilities.printComponent('customer-table')
  }

  exportCsv() {
    console.log(this.customers);
    let exportData = [];
    exportData.push({
      'Name': 'Contract Number',
      'Type': 'Type',
      'Phone': 'Phone',
      'Address': 'Address'
    });
    this.customers.map((item) => {
      exportData.push({
        'Contract Number': item.name,
        'Type': item.type,
        'Phone': item.customerPhoneBook[0].phone,
        'Address': `${item.locations[0].block}, ${item.locations[0].street}, ${item.locations[0].area}, ${item.locations[0].governorate}`
      })
    });
    return new Angular2Csv(exportData, 'Customers', {
      showLabels: true
    });
  }

}
