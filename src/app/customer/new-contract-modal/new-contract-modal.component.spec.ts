import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewContractModalComponent } from './new-contract-modal.component';

describe('NewContractModalComponent', () => {
  let component: NewContractModalComponent;
  let fixture: ComponentFixture<NewContractModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewContractModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewContractModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
