import {Component, OnInit, OnDestroy, ViewChild} from '@angular/core';
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {Subscription} from "rxjs";
// import {MenuItem} from
// import {MessageService} from 'primeng/components/common/messageservice';
import {Message} from "primeng/components/common/message";
// import {Router} from "@angular/router";
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {GenerateContractModalComponent} from "../generate-contract-modal/generate-contract-modal.component";
import {SearchComponent} from "../../shared-module/search/search.component";

@Component({
  selector: 'app-all-contracts',
  templateUrl: './all-contracts.component.html',
  styleUrls: ['./all-contracts.component.css'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})

export class AllContractsComponent implements OnInit, OnDestroy {
  allContracts: any;
  allContractSubscription: Subscription;
  // deleteContractSubscription: Subscription;
  buttonsList: any[];
  // activeRow: any;
  bigMessage: Message[] = [];
  cornerMessage: Message[] = [];
  toggleLoading: boolean;
  @ViewChild(SearchComponent)
  searchComponent: SearchComponent;

  constructor(private lookup: LookupService, private utilities: UtilitiesService, private modalService: NgbModal) {
  }

  ngOnInit() {
    this.allContracts = [];
    console.log(this.utilities.routingFromAndHaveSearch);
    if (this.utilities.routingFromAndHaveSearch && this.utilities.currentSearch.searchText && this.utilities.currentSearch.searchType == 'contract') {
      console.log('will search');
      this.searchComponent.searchText = this.utilities.currentSearch.searchText;
      this.searchContract(this.utilities.currentSearch.searchText);
    } else {
      console.log('get all');
      this.getAllContracts();
    }
  }

  ngOnDestroy() {
    this.allContractSubscription && this.allContractSubscription.unsubscribe();
  }

  openAddContractModal() {
    console.log('open modal');
    let contractModal = this.modalService.open(GenerateContractModalComponent);
    contractModal.result.then(() => {
      this.getAllContracts();
    })
      .catch(() => {

      })
  }


  printContracts(): void {
    var newWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=95%');

    var divToPrint = document.getElementById("contract-table");
    // let newWin = window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
    newWin.close();
  }

  getAllContracts() {
    this.toggleLoading = true;
    this.allContractSubscription = this.lookup.getAllContracts().subscribe((allContracts) => {
        this.toggleLoading = false;
        this.allContracts = allContracts;
        console.log('contracts');
        console.log(this.allContracts);
        this.bigMessage = [];
        this.bigMessage.push({
          severity: 'info',
          summary: 'Dropdown arrow',
          detail: 'You can remove any Contract by click the dropdown arrow and choose remove.'
        });
        //console.log(this.allContracts);
        // this.cd.markForCheck();
      },
      err => {
        this.toggleLoading = false;
        err.status == 401 && this.utilities.unauthrizedAction();
        this.cornerMessage = [];
        this.cornerMessage.push({
          severity: 'error',
          summary: 'Failed',
          detail: 'Failed to load Contracts due to server error!'
        });
      });
  }

  searchContract(searchText) {
    this.toggleLoading = true;
    console.log(searchText);
    this.utilities.currentSearch.searchType = 'contract';
    this.lookup.searchContract(searchText).subscribe((contracts) => {
        this.allContracts = contracts;
        console.log(this.allContracts);
        this.toggleLoading = false;
        if (!contracts.length) {
          this.cornerMessage.push({
            severity: "error",
            summary: "Failed",
            detail: "Result not found."
          });
        }
      },
      err => {
        this.cornerMessage.push({
          severity: "error",
          summary: "Failed",
          detail: "Failed to get data due to network error, please try again later."
        });
        this.toggleLoading = false;
      })
  }

}
