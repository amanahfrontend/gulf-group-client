import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
// import {Message} from "primeng/components/common/message";
// import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";

@Component({
  selector: 'app-add-edit-customer',
  templateUrl: './add-edit-customer.component.html',
  styleUrls: ['./add-edit-customer.component.css']
})
export class AddEditCustomerComponent implements OnInit, OnChanges {
  newCustomer: any;
  customerTypes: any[];
  governorates: any[];
  areas: any[];
  streets: any[];
  phoneTypes: any[];
  blocks: any[];
  cornerMessage: any = [];
  @Input() existedCustomerData: any;
  toggleLoadingLocation: boolean;
  // customerId: any;
  @Output() customerIdValue = new EventEmitter();
  @Output() closeFormNow = new EventEmitter();

  constructor(private lookup: LookupService) {
  }

  ngOnInit() {
    this.getCustomerType();
    this.getPhoneTypes();
    if (this.newCustomer== undefined) {
      this.newCustomer = {
        locations: [{}],
        customerPhoneBook: [{}]
      };
    }
    //this.newCustomer = this.existedCustomerData;
    this.getGovernorates();
  }

  ngOnChanges() {
    console.log(this.existedCustomerData);
    if (this.existedCustomerData) {
      // this.toggleLoadingLocation = true;
      this.newCustomer = this.existedCustomerData;
      this.getMultipleLocations(0, this.newCustomer.locations)
      console.log(this.newCustomer);
    }
  }

  getMultipleLocations(i, locations) {
    console.log(i);
    console.log(locations.length);
    if (i == locations.length) {
      console.log('recursive done!');
    } else {
      this.getGovernorates().then(() => {
        console.log('in gov success promise');
        this.getAreas(this.newCustomer.locations[i]).then(() => {
          console.log('in area success promise');
          this.getBlocks(this.newCustomer.locations[i]).then(() => {
            console.log('in blocks success promise');
            this.getStreets(this.newCustomer.locations[i]).then(() => {
              this.getMultipleLocations(++i, locations)
            })
          })
        })
      });
    }
  }

  addNewLocation() {
    this.newCustomer.locations.push({ isNewLocation: true });
  }

  addNewPhone() {
    this.newCustomer.customerPhoneBook.push({});
    console.log(this.newCustomer.customerPhoneBook);
  }

  removePhone(index) {
    console.log(index);
    this.newCustomer.customerPhoneBook.splice(index, 1);
  }

  saveLocation(loc) {
    console.log(loc);
    let locationToPost = {
      "PACINumber": loc.paciNumber,
      "governorate": loc.governorate,
      "area": loc.area,
      "block": loc.block,
      "street": loc.street,
      "title": loc.title,
      "addressNote": loc.addressNote,
      "fk_Customer_Id": this.existedCustomerData.id,
      "id": loc.id
    };
    console.log(locationToPost);
    if (loc.isNewLocation) {
      this.lookup.postNewLocation(locationToPost).subscribe(() => {
        console.log('success');
        this.cornerMessage.push({
          severity: "success",
          summary: "Successfully!",
          detail: "Location Added Successfully!"
        });
        setTimeout(() => {
          this.cornerMessage = [];
        }, 2000)
      },
        err => {
          console.log('failed');
        })
    } else {
      this.lookup.updateLocation(locationToPost).subscribe(() => {
        console.log('success');

        console.log('success');
        this.cornerMessage.push({
          severity: "success",
          summary: "Successfully!",
          detail: "Location Saved Successfully!"
        });
        setTimeout(() => {
          this.cornerMessage = [];
        }, 2000)
      },
        err => {
          console.log('failed');
        })
    }
  }

  saveCustomerInfo(info) {
    console.log(info);
    let customerPhoneBookToPost = [];
    info.customerPhoneBook.map((singlePhoneBook) => {
      customerPhoneBookToPost.push({
        // id: info.id,
        phone: singlePhoneBook.phone,
        fK_PhoneType_Id: singlePhoneBook.fK_PhoneType_Id
      });
    });
    let infoPostObject = {
      id: info.id,
      name: info.name,
      remarks: info.remarks,
      customerPhoneBook: customerPhoneBookToPost,
      fK_CustomerType_Id: info.fK_CustomerType_Id
    };
    console.log(infoPostObject);
    this.lookup.updateCustomer(infoPostObject).subscribe(() => {
      console.log('success');
    },
      err => {
        console.log('failed');
      })
  };

  closeForm() {
    this.closeFormNow.emit();
  }

  getPhoneTypes() {
    this.lookup.getPhoneTypes().subscribe((phoneTypes) => {
      this.phoneTypes = phoneTypes;
    },
      err => {

      })
  }

  getCustomerType() {
    this.lookup.getCustomerTypes().subscribe((customerTypes) => {
      this.customerTypes = customerTypes;
    },
      err => {

      })
  }

  postNewCustomer() {
    console.log(this.newCustomer);
    this.lookup.postCustomer(this.newCustomer).subscribe((res) => {
      // this.customerId = res;
      this.customerIdValue.emit(res);
      console.log('success');
    },
      err => {
        console.log('failed');
      });
    // console.log(postObj);
  }

  getGovernorates() {
    return new Promise((resolve, reject) => {
      // (function internalRecursive() {
      // }());
      this.lookup.GetallGovernorates().subscribe((govs) => {
        if (govs[0].name == 'Not Found In Paci') {
          console.log('will retry');
          this.getGovernorates();
        } else {
          console.log('got governorates');
          this.governorates = govs;
          resolve();
        }
      },
        err => {
          console.log(err);
        })
    })
  }

  getAreas(singleLocation) {
    console.log('in getting areas');
    return new Promise((resolve, reject) => {
      let id = this.selectId(singleLocation.governorate, this.governorates);
      console.log(id);
      var lookup = this.lookup;
      (function internalRecursive() {
        lookup.GetallAreas(id).subscribe((areas) => {
          if (areas[0].name == 'Not Found In Paci') {
            console.log('will retry Areas');
            internalRecursive();
          } else {
            singleLocation.areas = areas;
            resolve();
          }
        })
      }());
    })
  }

  getBlocks(singleLocation) {
    return new Promise((resolve, reject) => {
      let id = this.selectId(singleLocation.area, singleLocation.areas);
      console.log(id);
      var lookUp = this.lookup;
      (function internalRecursive() {
        lookUp.GetallBlocks(id).subscribe((blocks) => {
          if (blocks[0].name == 'Not Found In Paci') {
            console.log('will retry blocks');
            internalRecursive();
          } else {
            singleLocation.blocks = blocks;
            resolve();
          }
        },
          err => {
            console.log('failed');
          })
      })();

    })
  }

  getStreets(singleLocation) {
    return new Promise((resolve, reject) => {
      let govId = this.selectId(singleLocation.governorate, this.governorates);
      let areaId = this.selectId(singleLocation.area, singleLocation.areas);
      var lookUp = this.lookup;
      (function internalRecursive() {
        lookUp.GetallStreets(areaId, singleLocation.block, govId).subscribe((streets) => {
          // this.streets = streets;
          if (streets[0].name == 'Not Found In Paci') {
            console.log('will retry streets');
            internalRecursive();
          } else {
            console.log('got streets');
            singleLocation.streets = streets;
            resolve();
          }
        },
          err => {
            console.log('failed');
          })
      })();

    })
  }

  getByPaci(locationIndex) {
    if (this.newCustomer.locations[locationIndex].paciNumber >= 8) {
      console.log('getting location by PACI');
      this.lookup.GetLocationByPaci(this.newCustomer.locations[locationIndex].paciNumber).subscribe((location) => {
        console.log(location);
        if (location.governorate) {
          this.newCustomer.locations[locationIndex].governorate = location.governorate.name;
        }
        if (location.area) {
          this.newCustomer.locations[locationIndex].areas = [{ name: location.area.name }];
          this.newCustomer.locations[locationIndex].area = location.area.name;
        }
        if (location.block) {
          this.newCustomer.locations[locationIndex].blocks = [{ name: location.block.name }];
          this.newCustomer.locations[locationIndex].block = location.block.name;
        }
        if (location.street) {
          this.newCustomer.locations[locationIndex].streets = [{ name: location.street.name }];
          this.newCustomer.locations[locationIndex].street = location.street.name;
        }
      },
        err => {
          console.log(err);
        })
    } else {

    }

  }

  selectId(name, list) {

    if (name != undefined) {
      console.log(name);
      console.log(list);
      return list.filter((item) => {
        return item.name == name;
      })[0].id;
    }


  }

}
