import { Component, Input, OnChanges, OnDestroy } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { MessageService } from "primeng/components/common/messageservice";
import { Subscription } from "rxjs";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { AddEditUserModalComponent } from "./add-edit-user-modal/add-edit-user-modal.component";
import { AdminChangePasswordComponent } from "./admin-change-password/admin-change-password.component";

@Component({
  selector: "app-admin-users",
  templateUrl: "./admin-users.component.html",
  styleUrls: ["./admin-users.component.css"]
})

export class AdminUsersComponent implements OnDestroy, OnChanges {
  @Input() getData: boolean;
  users: any[];
  UsersSubscription: Subscription;
  modalRef: any;
  toggleLoading;

  constructor(private lookup: LookupService, private messageService: MessageService, private modalService: NgbModal) {
  }

  // ngOnInit() {
  //   this.toggleLoading = true;
  // }

  ngOnChanges() {
    this.toggleLoading = true;

    if (this.getData) {
      this.UsersSubscription = this.lookup.getUsers().subscribe(
        (users) => {
          this.toggleLoading = false;
          this.users = users;
          this.users.map((user) => {
            user.name = `${user.firstName} ${user.middleName} ${user.lastName}`;
          });
          //console.log('got users');
        },
        err => {
          this.toggleLoading = false;

          this.messageService.add({
            severity: "error",
            summary: "Failed",
            detail: `Failed to load data due to server error.`
          });
        });
    }
  }

  ngOnDestroy() {
    this.UsersSubscription && this.UsersSubscription.unsubscribe();
  }

  edit(user) {
    let userCopy = Object.assign({}, user);

    this.openAddEditUserModal(userCopy, `Edit ${userCopy.name} info`);

    this.modalRef.result.then(
      (editedData) => {
        let userRoles = [];
        userRoles.push(editedData.roleNames);
        editedData.roleNames = userRoles;

        delete editedData.name;
        console.log(editedData);
        console.log(editedData.roleNames);

        this.lookup.updateUser(editedData).subscribe(
          () => {
            this.messageService.add({
              severity: "success",
              summary: "Successful!",
              detail: `${user.name} info Edited & Saved Successfully!`
            });
            user = editedData;
          },
          err => {
            this.messageService.add({
              severity: "error",
              summary: "Failed",
              detail: `Failed to edit ${user.name} info due to server error.`
            });
          });
      })
      .catch(() => {
        this.messageService.add({
          severity: "info",
          summary: "Nothing Edited!",
          detail: `No edits saved to ${user.name} info`
        });
      });
  }

  changeUserPassword(user) {
    let userCopy = Object.assign({}, user);
    this.openModalChangePassword(userCopy, `Change ${userCopy.name} password`);

  }

  remove(user) {
    //console.log(user);
    this.lookup.deleteUser(user.userName).subscribe(
      () => {
        this.messageService.add({
          severity: "success",
          summary: "Successful!",
          detail: "User deleted Successfully!"
        });

        this.users = this.users.filter((oneUser) => {
          return oneUser.userName !== user.userName;
        });
      },
      err => {
        this.messageService.add({
          severity: "error",
          summary: "Failed!",
          detail: "Failed to delete user due to network error, please try again later."
        });
      });
  }

  add() {
    this.openAddEditUserModal({}, `Add new user`);

    this.modalRef.result.then(
      (newUser) => {
        let userRoles = [];
        //console.log(newUser.roleNames);
        userRoles.push(newUser.roleNames);
        newUser.roleNames = userRoles;
        //console.log(newUser.roleNames);

        this.lookup.postNewUser(newUser).subscribe(
          (resUser) => {
            console.log(resUser);

            this.messageService.add({
              severity: "success",
              summary: "saved successfully!",
              detail: `New user ${resUser.firstName} ${resUser.middleName}${resUser.lastName} saved successfully!`
            });

            resUser.name = resUser.firstName + resUser.middleName + resUser.lastName;
            this.users.push(resUser);
          },
          err => {
            this.messageService.add({
              severity: "error",
              summary: "Failed",
              detail: `Failed to save new user due to server error.`
            });
          });
      })
      .catch(() => {
        this.messageService.add({
          severity: "info",
          summary: "User canceled",
          detail: `New user didn't saved and canceled.`
        });
      });
  }

  openAddEditUserModal(data, header) {
    this.modalRef = this.modalService.open(AddEditUserModalComponent);
    this.modalRef.componentInstance.header = header;
    // this.modalRef.componentInstance.type = 'prompt';
    this.modalRef.componentInstance.data = data;
  }

  openModalChangePassword(data, header) {
    this.modalRef = this.modalService.open(AdminChangePasswordComponent);
    this.modalRef.componentInstance.header = header;
    this.modalRef.componentInstance.data = data;
  }
}
