import {Component, OnInit, Input} from "@angular/core";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {LookupService} from "../../../api-module/services/lookup-services/lookup.service";

@Component({
  selector: 'app-edit-dis-and-prob-modal',
  templateUrl: './edit-dis-and-prob-modal.component.html',
  styleUrls: ['./edit-dis-and-prob-modal.component.css']
})
export class EditDisAndProbModalComponent implements OnInit {
  @Input() data;
  problems: any[];
  dispatcherDetails: any;
  dispatcherDetailsFormated: any[];
  areas: any[];
  governorates: any[];
  toggleLoading: boolean;
  newProblems: any[];
  cornerMessage: any[];

  constructor(private activeModal: NgbActiveModal, private _lookup: LookupService) {
  }

  ngOnInit() {
    this.newProblems = [];
    this.dispatcherDetails = {};
    this.dispatcherDetailsFormated = [];
    console.log(this.data);
    this.getAllProblems();
    if (this.data.area != '-') {
      this.getDispatcherAllProblems(this.data.fK_Dispatcher_Id);
      this.toggleLoading = true;
    }
    this.getGovernorate();
    console.log(this.data)
  }

  addNewProblem() {
    console.log('NEW PROBLEM');
    console.log(this.newProblems);
    this.newProblems.push({
      governorate: '',
      area: '',
      problems: []
    });
    console.log(this.newProblems);
  }

  removeNewProblem(item) {
    let removedFlag: boolean;
    this.newProblems.map((problem) => {
      if (JSON.stringify(item) == JSON.stringify(problem) && !removedFlag) {
        this.newProblems.splice(this.newProblems.indexOf(item), 1)
      }
    })
  }

  getGovernorate() {
    this._lookup.GetallGovernorates().subscribe((governorates) => {
        if (governorates[0].name == 'Not Found In Paci') {
          console.log('will retry');
          this.getGovernorate();
        } else {
          console.log('got governorates');
          this.governorates = governorates;
          if (this.dispatcherDetailsFormated.length) {
            this.dispatcherDetailsFormated.map((dispatcherDetail) => {
              this.getAreas(dispatcherDetail);
            })
          } else {
            this.newProblems.map((prob) => {
              this.getAreas(prob);
            })
          }
        }
      },
      err => {

      })
  }

  getAreas(item) {
    let usedItem = item;
    console.log(usedItem);
    // this.toggleLoading = true;
    let id = this.selectId(usedItem.governorate, this.governorates);
    console.log(id);
    this._lookup.GetallAreas(id).subscribe((areas) => {
        if (areas[0].name == 'Not Found In Paci') {
          console.log('will retry Areas');
          this.getAreas(usedItem);
        } else {
          // console.log('got Areas');
          // console.log(areas);
          if (this.dispatcherDetailsFormated.length) {
            this.dispatcherDetailsFormated.map((dis) => {
              if (dis.governorate == usedItem.governorate) {
                dis.areas = areas;
                this.toggleLoading = false;
              }
            });
          }
          else {
            this.newProblems.map((prob) => {
              if (prob.governorate == usedItem.governorate) {
                prob.areas = areas;
                this.toggleLoading = false;
              }
            })
          }
          if (this.newProblems.length) {
            this.newProblems.map((prob) => {
              if (prob.governorate == usedItem.governorate) {
                prob.areas = areas;
                this.toggleLoading = false;
              }
            })
          }
          // console.log(usedItem.areas);
          console.log(this.dispatcherDetailsFormated);
        }
      },
      err => {

      });
  }

  selectId(name, list) {
    console.log(name);
    console.log(list);
    return list.filter((item) => {
      return item.name == name;
    })[0].id;
  }

  getAllProblems() {
    this._lookup.getAllProblems().subscribe((probs) => {
        this.problems = probs;
        console.log(this.problems)
        this.problems.map((prob, i) => {
          this.problems[i] = {
            id: prob.id,
            name: prob.name
          }
        })
      },
      err => {

      })
  }

  getDispatcherAllProblems(id) {
    this._lookup.getDispatcherAllProblems(id).subscribe((disWithAllProbs) => {
        this.dispatcherDetails = disWithAllProbs;
        this.dispatcherDetails.map((detail) => {
          console.log(detail);
          let area = detail.area;
          if (this.dispatcherDetailsFormated.length) {
            let areaFoundFlag = false;
            this.dispatcherDetailsFormated.map((detailFormated, i) => {
              if (detailFormated.area == area) {
                detailFormated.problems.push({
                  id: detail.orderProblem.id,
                  name: detail.orderProblem.name
                });
                areaFoundFlag = true;
              }
              if (!areaFoundFlag && (i == this.dispatcherDetailsFormated.length - 1)) {
                this.dispatcherDetailsFormated.push({
                  area: detail.area,
                  governorate: detail.governorate,
                  problems: [
                    {
                      id: detail.orderProblem.id,
                      name: detail.orderProblem.name
                    }
                  ]
                })
              }
            });
          } else {
            this.dispatcherDetailsFormated.push({
              area: detail.area,
              governorate: detail.governorate,
              problems: [
                {
                  id: detail.orderProblem.id,
                  name: detail.orderProblem.name
                }
              ]
            })
          }
        });
        // this.selectedProbs = this.dispatcherDetailsFormated[0].problems;
        // console.log(this.dispatcherDetails);
        console.log(this.dispatcherDetailsFormated);
      },
      err => {

      })
  }

  applyEdits() {
    console.log(this.dispatcherDetailsFormated);
    console.log(this.newProblems);
    let allViewProblemsAndAreas = this.dispatcherDetailsFormated.concat(this.newProblems);
    let dispatcherToPost = [];
    let areaProblems = [];
    allViewProblemsAndAreas.map((area) => {
      let fK_OrderProblem_Ids = [];
      area.problems.map((problem, i) => {
        console.log(area.problems.length);
        console.log(i);
        fK_OrderProblem_Ids.push(problem.id);
        if (area.problems.length - 1 == i) {
          areaProblems.push({
            area: area.area,
            governorate: area.governorate,
            fK_OrderProblem_Ids: fK_OrderProblem_Ids
          })
        }
      });
    });

    // console.log(this.data);

    dispatcherToPost.push({
      fK_Dispatcher_Id: this.data.dispatcher.id,
      areaProblems: areaProblems
    });

    console.log(dispatcherToPost);

    this._lookup.postDispatcherWithProblemsAndAreas(dispatcherToPost).subscribe(() => {
        console.log('successful');
        this.activeModal.close();
        // location.reload();
      },
      err => {
        console.log('failed')
      })
  }

  close() {
    this.activeModal.dismiss();
  }
}
