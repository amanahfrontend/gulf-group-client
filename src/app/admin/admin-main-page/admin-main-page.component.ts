import {Router} from '@angular/router';
import {Component, OnInit, Input} from '@angular/core';
import {Message} from "primeng/components/common/message";

@Component({
  selector: 'app-admin-main-page',
  templateUrl: './admin-main-page.component.html',
  styleUrls: ['./admin-main-page.component.css', '../../customer/existing-customer/existing-customer.component.css']
})

export class AdminMainPageComponent implements OnInit {
  getDataForType: string;
  items: string[];
  activeTab: string;
  getUserDataFlag: boolean;
  msg: Message[];

  constructor(private router: Router) {
  }

  ngOnInit() {
    this.items = ['Customer Type', 'Call Type', 'Call Priority', 'Phone Type', 'Action Status', 'Contract Type', 'Order Type', 'Order Priority', 'Order Status', 'Order Problems', 'Order Progress'];
    this.getUserDataFlag = false;
    this.activeTab = 'data';
    this.getData('Customer Type');
  }

  setActiveTab(tabName) {
    this.activeTab = tabName;
    if (tabName == 'users') {
      this.getUserDataFlag = true;
    } else if (tabName == 'upload') {
      this.getUserDataFlag = false;
    } else if (tabName == 'assign-tec') {
      this.getUserDataFlag = false;
    } else if (tabName == 'settingDispatchers') {
      this.getUserDataFlag = false;
    } else if (tabName == 'data') {
      this.getUserDataFlag = false;
    }
  }

  applyDefaultActiveClass(item) {
    if (this.items.indexOf(item) == 0) {
      return 'uk-open'
    }
  }

  getData(type) {
    this.getDataForType = type;
    console.log(this.getDataForType);
  }

}
