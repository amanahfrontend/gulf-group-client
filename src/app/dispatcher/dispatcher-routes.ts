import {DispatcherOrderBoardComponent} from './dispatcher-order-board/dispatcher-order-board.component'
import {AuthGuardGuard} from "../api-module/guards/auth-guard.guard";
import {ReportComponent} from "./report/report.component";
import {PreventiveComponent} from './preventive/preventive/preventive.component';
import {VisitTimeCalenderComponent} from "./visit-time-calender/visit-time-calender.component";

export const dispatcherRoutes = [
  {
    path: '',
    component: DispatcherOrderBoardComponent,
    canActivate: [AuthGuardGuard],
    data: {roles: ['Dispatcher', 'SupervisorDispatcher']}
  },
  {
    path: 'report',
    component: ReportComponent,
    canActivate: [AuthGuardGuard],
    data: {roles: ['Dispatcher', 'SupervisorDispatcher']}
  },
  {
    path: 'calender',
    component: VisitTimeCalenderComponent,
    canActivate: [AuthGuardGuard],
    data: {roles: ['Dispatcher', 'SupervisorDispatcher']}
  },
  {
    path: 'preventive',
    component: PreventiveComponent,
    canActivate: [AuthGuardGuard],
    data: {roles: ['Dispatcher', 'SupervisorDispatcher']}
  }
];
